#!/bin/bash

# GNOME Settings & Tweaks
## Enable Dark Mode
gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
## Disable Hot Corner
gsettings set org.gnome.desktop.interface enable-hot-corners false
## Custom keyboard shortcut to launch terminal
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name "'gnome-terminal'"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding "'<Super>Return'"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command "'gnome-terminal'"
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/']"
## Change clock format to AM/PM
gsettings set org.gnome.desktop.interface clock-format '12h'
gsettings set org.gtk.Settings.FileChooser clock-format '12h'
gsettings set org.gtk.gtk4.Settings.FileChooser clock-format '12h'
## Show weekday on top bar
gsettings set org.gnome.desktop.interface clock-show-weekday true
## Show all window buttons
gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'
## Enable audio over-amplification
#gsettings set org.gnome.desktop.sound allow-volume-above-100-percent 'true'
## Enable tap to click
gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
## Power button behaviour set to power off
gsettings set org.gnome.settings-daemon.plugins.power power-button-action 'interactive'

# Improving the DNF package manager
sudo bash -c "echo 'max_parallel_downloads=10' >> /etc/dnf/dnf.conf"

# Update the system
sudo dnf upgrade -y

# Remove DNF packages
sudo dnf remove -y --skip-broken gnome-calendar
sudo dnf remove -y --skip-broken gnome-weather
sudo dnf remove -y --skip-broken gnome-clocks
sudo dnf remove -y --skip-broken gnome-maps
sudo dnf remove -y --skip-broken gnome-tour
sudo dnf remove -y --skip-broken gnome-connections
sudo dnf remove -y --skip-broken gnome-contacts
sudo dnf remove -y --skip-broken gnome-calculator
sudo dnf remove -y --skip-broken gnome-boxes
sudo dnf remove -y --skip-broken libreoffice-calc
sudo dnf remove -y --skip-broken libreoffice-impress
sudo dnf remove -y --skip-broken libreoffice-writer
sudo dnf remove -y --skip-broken yelp
sudo dnf remove -y --skip-broken rhythmbox
sudo dnf remove -y --skip-broken cheese


# Install RPM Fusion Repositories
#sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
#sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm

# Install DNF packages
sudo dnf install -y \
#epiphany \
#gimp \
#inkscape \
#scribus \
#godot \
#steam \
gnome-tweaks \
neofetch \
#gnome-shell-extension-pop-shell \
#gnome-shell-extension-drive-menu \
#gnome-shell-extension-gsconnect \
#gnome-shell-extension-caffeine \
/

# Clean up unused packages
sudo dnf autoremove -y

# Add flathub remote for flatpak
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Enable repositories
sudo flatpak remote-modify --enable fedora-testing
sudo flatpak remote-modify --enable flathub

# Disable repositories
sudo dnf config-manager --disable google-chrome rpmfusion-nonfree-nvidia-driver copr:copr.fedorainfracloud.org:phracek:PyCharm

# Install flatpak packages
#flatpak install flathub -y org.onlyoffice.desktopeditors
#flatpak install flathub -y com.vscodium.codium
flatpak install flathub -y com.synology.SynologyDrive
flatpak install flathub -y com.mattjakeman.ExtensionManager
#flatpak install flathub -y com.discordapp.Discord
#flatpak install flathub -y com.github.libresprite.LibreSprite

## Autostart applications
cp /var/lib/flatpak/exports/share/applications/com.synology.SynologyDrive.desktop ~/.config/autostart/SynologyDrive.desktop
